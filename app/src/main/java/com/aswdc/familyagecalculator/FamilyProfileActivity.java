package com.aswdc.familyagecalculator;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aswdc.familyagecalculator.adapter.UserListAdapter;
import com.aswdc.familyagecalculator.database.TblUser;
import com.aswdc.familyagecalculator.model.UserModel;
import com.aswdc.familyagecalculator.util.Constant;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FamilyProfileActivity extends BaseActivity {

    @BindView(R.id.rcvUserList)
    RecyclerView rcvUserList;
    DatePickerDialog picker;
    TextView txtName, txtRelation, txtDate;

    ArrayList<UserModel> userList = new ArrayList<>();
    UserListAdapter adapter;
    UserModel userModel;

    int tempMonth, tempYear, tempDay;

    int average;
    @BindView(R.id.familyprofile_tvavergae)
    TextView familyprofileTvavergae;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.familyprofile);
        setUpActionBar("Family Age Calculator", true);
        ButterKnife.bind(this);
        setAdapter();
        getDataForUpdate();
    }

    void setAdapter() {
        rcvUserList.setLayoutManager(new GridLayoutManager(this, 1));
        userList.addAll(new TblUser(this).getUserList());
        calculateAverage();
        adapter = new UserListAdapter(this, userList, new UserListAdapter.OnViewClickListener() {
            @Override
            public void OnDeleteClick(int position) {

            }

            @Override
            public void OnItemClick(int position) {

            }
        });
        rcvUserList.setAdapter(adapter);
    }

    void calculateAverage() {
        average = 0;

        for (int i = 0; i < userList.size(); i++) {
            average += userList.get(i).getAge();
        }

        average /= userList.size();

        familyprofileTvavergae.setText(String.valueOf(average));
    }

    void getDataForUpdate() {
        if (getIntent().hasExtra(Constant.USER_OBJECT)) {
            userModel = (UserModel) getIntent().getSerializableExtra(Constant.USER_OBJECT);
            getSupportActionBar().setTitle("Edit User");
            txtName.setText(userModel.getName());
            txtRelation.setText(userModel.getRelation());
            txtDate.setText(userModel.getDate());
        }
    }

    public void btn_showMessage(View view) {
        final AlertDialog.Builder alert = new AlertDialog.Builder(FamilyProfileActivity.this);
        View mView = getLayoutInflater().inflate(R.layout.familyprofileform, null);

        final EditText txt_inputText = (EditText) mView.findViewById(R.id.etName);
        final EditText txt_inputRelation = (EditText) mView.findViewById(R.id.etRelation);
        final EditText txt_inputDate = (EditText) mView.findViewById(R.id.etDate);
        txt_inputDate.setInputType(InputType.TYPE_NULL);
        txt_inputDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Calendar cldr = Calendar.getInstance();
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);
                // date picker dialog
                picker = new DatePickerDialog(FamilyProfileActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                txt_inputDate.setText(String.format("%02d", dayOfMonth) + "-" + String.format("%02d", (monthOfYear + 1)) + "-" + year);
                                tempDay = dayOfMonth;
                                tempMonth = monthOfYear;
                                tempYear = year;
                            }
                        }, year, month, day);
                picker.show();
            }
        });


        Button btn_cancel = (Button) mView.findViewById(R.id.btn_cancel);
        Button btn_okay = (Button) mView.findViewById(R.id.btn_okay);
        alert.setView(mView);

        final AlertDialog alertDialog = alert.create();
        alertDialog.setCanceledOnTouchOutside(false);

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        btn_okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                long lastInsertedID = new TblUser(getApplicationContext()).insertUserByID(txt_inputText.getText().toString(),
                        txt_inputRelation.getText().toString(), txt_inputDate.getText().toString(), tempDay, tempMonth, tempYear);

                alertDialog.dismiss();

                Intent intent = new Intent(FamilyProfileActivity.this, FamilyProfileActivity.class);
                startActivity(intent);

            }
        });

        alertDialog.show();
    }
}


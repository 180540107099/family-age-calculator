package com.aswdc.familyagecalculator;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aswdc.familyagecalculator.adapter.UserListAdapter;
import com.aswdc.familyagecalculator.database.TblUser;
import com.aswdc.familyagecalculator.model.UserModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UserListActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rcvUserList)
    RecyclerView rcvUserList;

    ArrayList<UserModel> userList = new ArrayList<>();
    UserListAdapter adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);
        ButterKnife.bind(this);
        setAdapter();
    }

    void setAdapter() {
        rcvUserList.setLayoutManager(new GridLayoutManager(this, 1));
        userList.addAll(new TblUser(this).getUserList());
        adapter = new UserListAdapter(this, userList, new UserListAdapter.OnViewClickListener() {
            @Override
            public void OnDeleteClick(int position) {
                int lastUpdatedUserID = new TblUser(UserListActivity.this).deleteUserByID(0 );
                if (lastUpdatedUserID > 0) {
                    userList.remove(position);
                    adapter.notifyItemRemoved(position);
                    adapter.notifyItemRangeChanged(0, userList.size());
                }
            }

            @Override
            public void OnItemClick(int position) {

            }
        });
        rcvUserList.setAdapter(adapter);
    }
}

package com.aswdc.familyagecalculator;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;

public class FrontLayoutActivity extends BaseActivity {

    TextView tvCalculateAge, tvDateDifference, tvFamilyProfile, tvCompareAge;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.frontlayout);
        setUpActionBar("Family Age Calculator",false);
        initViewReference();
        initViewEvent();
    }

    void initViewEvent() {
        tvCalculateAge.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent calculateAgeIntent = new Intent(FrontLayoutActivity.this, CalculateAgeActivity.class);
                startActivity(calculateAgeIntent);
            }
        });

        tvDateDifference.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent dateDifferenceIntent = new Intent(FrontLayoutActivity.this, DateDifferenceActivity.class);
                startActivity(dateDifferenceIntent);
            }
        });

        tvFamilyProfile.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent familyProfileIntent = new Intent(FrontLayoutActivity.this, FamilyProfileActivity.class);
                startActivity(familyProfileIntent);

            }
        });

        tvCompareAge.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent compareAgeIntent = new Intent(FrontLayoutActivity.this, CompareAgeActivity.class);
                startActivity(compareAgeIntent);
            }
        });

    }

    void initViewReference() {
        tvCalculateAge = findViewById(R.id.tvActCalculateAge);
        tvDateDifference = findViewById(R.id.tvActDateDifference);
        tvFamilyProfile = findViewById(R.id.tvActFamilyProfile);
        tvCompareAge = findViewById(R.id.tvActCompareAge);
    }
}

package com.aswdc.familyagecalculator;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.RequiresApi;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class CalculateAgeActivity extends BaseActivity {
    DatePickerDialog picker;
    EditText eText1,eText2,eText3;
    Button btnGet;
    TextView tvDays, tvMonths, tvYears, tvHours, tvWeeks, tvMinutes, tvMonth, tvDay;
    String format;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calculateage);
        setUpActionBar("Family Age Calculator",true);

        eText1 = (EditText) findViewById(R.id.etStart);
        eText1.setInputType(InputType.TYPE_NULL);
        eText1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Calendar cldr = Calendar.getInstance();
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);
                // date picker dialog
                picker = new DatePickerDialog(CalculateAgeActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                eText1.setText(String.format("%02d",dayOfMonth) + "-" + String.format("%02d",(monthOfYear + 1)) + "-" + year);
                            }
                        }, year, month, day);
                picker.show();
            }
        });

        eText2 = (EditText) findViewById(R.id.etEnd);
        eText2.setInputType(InputType.TYPE_NULL);
        eText2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);

                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(CalculateAgeActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                        if (selectedHour == 0) {
                            selectedHour += 12;
                            format = "AM";
                        }
                        else if (selectedHour == 12) {
                            format = "PM";
                        }
                        else if (selectedHour > 12) {
                            selectedHour -= 12;
                            format = "PM";
                        }
                        else {
                            format = "AM";
                        }

                        eText2.setText(new StringBuilder().append(String.format("%02d",selectedHour)).append(" : ").append(String.format("%02d",selectedMinute))
                                .append(" ").append(format));
                    }
                }, hour, minute, false);
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });

    //    private void nextBirthday() {
    //        int currentDay = Integer.valueOf(editTextCurrentDay.getText().toString());
    //        int currentMonth = Integer.valueOf(editTextCurrentMonth.getText().toString());
    //        int currentYear = Integer.valueOf(editTextCurrentYear.getText().toString());

    //        Calendar current = Calendar.getInstance();
    //        current.set(currentYear, currentMonth, currentDay);

    //        int birthDay = Integer.valueOf(.getText().toString());
    //        int birthMonth = Integer.valueOf(editTextBirthMonth.getText().toString());
    //        int birthYear = Integer.valueOf(editTextBirthYear.getText().toString());

    //        Calendar birthday = Calendar.getInstance();
    //        birthday.set(birthYear, birthMonth, birthDay);

    //        long difference = birthday.getTimeInMillis() - current.getTimeInMillis();

    //        Calendar cal = Calendar.getInstance();
    //        cal.setTimeInMillis(difference);

    //        tvMonth.setText(String.valueOf(cal.get(Calendar.MONTH)));
    //        tvDay.setText(String.valueOf(cal.get(Calendar.DAY_OF_MONTH)));



        btnGet = (Button) findViewById(R.id.btnCalculate);
        btnGet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    Date date1;
                    Time time1;
                    SimpleDateFormat dates = new SimpleDateFormat("dd-MM-yyyy");

                    date1 = dates.parse(eText1.getText().toString());

                    long difference = Math.abs(date1.getTime());
                    long differenceDates = (difference / (24 * 60 * 60 * 1000) % 365);
                    long differenceMonths =(difference / 365 % 12) ;
                    long differenceYears = (difference / (10001 * 60 * 60 * 24 * 365) % 12);
                    long differenceWeeks = (difference / (60 * 60 * 24 * 1000));
                    long differenceMinutes = (difference / (60 * 1000));
                    long differenceHours = (difference / (60 * 60 * 1000));

                    String dayDifference = Long.toString(differenceDates);
                    String monthDifference = Long.toString(differenceMonths);
                    String yearDifference = Long.toString(differenceYears);
                    String weeksDifference = Long.toString(differenceWeeks);
                    String hoursDifference = Long.toString(differenceHours);
                    String minutesDifference = Long.toString(differenceMinutes);
                    tvDays.setText("" +dayDifference);
                    tvMonths.setText("" +monthDifference);
                    tvYears.setText("" +yearDifference);
                    tvHours.setText("" +hoursDifference);
                    tvMinutes.setText("" +minutesDifference);
                    tvWeeks.setText("" +weeksDifference);
                }
                catch (Exception exception) {
                    exception.printStackTrace();
                }
            }
        });

        Button btn=(Button) findViewById(R.id.btnClear);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText eText1=(EditText) findViewById(R.id.etStart);
                EditText eText2=(EditText) findViewById(R.id.etEnd);
                TextView tvDays=(TextView) findViewById(R.id.tvResultDays);
                eText1.setText(""); eText2.setText(""); tvDays.setText("");
            }
        });
    }


}
